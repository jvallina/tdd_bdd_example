import unittest

from src.partida import Partida, MissingNameException, Tirada


class PartidaTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        super().tearDownClass()

    def setUp(self):
        self.nombre_elegido = "mi partida de prueba"
        self.partida = Partida(self.nombre_elegido)

    def tearDown(self):
        pass

    def test_crear_partida_con_nombre_correcto(self):
        self.assertIsNotNone(self.partida)

    def test_comprobar_nombre_correcto(self):
        self.assertEqual(self.nombre_elegido, self.partida.nombre)

    def test_crear_partida_sin_nombre(self):
        with self.assertRaises(MissingNameException):
            nueva_partida = Partida()
            self.assertIsNone(nueva_partida)

    def test_comprbar_partida_vacia(self):
        self.assertEqual(0, self.partida.suma_partida)

    # US 2

    def test_anotar_tirada(self):
        tirada = Tirada(2, 3)
        nueva_partida = Partida('Partida de Prueba')
        nueva_partida.anotar_tirada(tirada)
        ultima_tirada = nueva_partida.ultima_tirada
        self.assertEqual(5, ultima_tirada.suma)



if __name__ == '__main__':
    unittest.main()
