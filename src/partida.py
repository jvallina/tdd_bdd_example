class MissingNameException(Exception):
    pass


class Tirada:
    def __init__(self, tirada_uno: int, tirada_dos: int):
        self.tirada_uno = tirada_uno
        self.tirada_dos = tirada_dos

    @property
    def suma(self) -> int:
        return self.tirada_uno + self.tirada_dos


class Partida:

    def __init__(self, nombre: str = None):
        if not nombre:
            raise MissingNameException('Falta poner le nombre')

        self.nombre = nombre
        self.suma_partida = 0
        self.tiradas = []

    def anotar_tirada(self, tirada: Tirada):
        self.tiradas.append(tirada)

    @property
    def ultima_tirada(self):
        return self.tiradas[-1]
